<?php
/**
 * Plugin Configuration
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'GuteBotschafter.' . $_EXTKEY,
	'Main',
	[
		'Glossary' => 'list, subset, show'
    ],
	[
		'Glossary' => 'subset'
    ]
);
/*
 * Register TCE helper to limit string length
 */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tce']['formevals']['tx_gbglossary_utility_evalstringlength'] = 'EXT:gb_glossary/Classes/Utility/EvalStringLength.php';
