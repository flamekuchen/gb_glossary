<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

/**
 * Plugin Configuration
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'GuteBotschafter.' . $_EXTKEY,
	'Main',
	'LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:labels.extensionName'
);

/**
 * TypoScript Setup
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Gute Botschafter Glossary');

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['gbglossary_main'] = 'layout,select_key,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['gbglossary_main'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('gbglossary_main', 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/Main.xml');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_gbglossary_domain_model_definition');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToInsertRecords('tx_gbglossary_domain_model_definition');
