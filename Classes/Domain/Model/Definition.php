<?php
namespace GuteBotschafter\GbGlossary\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2009-2015 Gute Botschafter GmbH, Morton Jonuschat <m.jonuschat@gute-botschafter.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class Definition extends AbstractEntity
{
    /**
     * @var string
     */
    protected $short;

    /**
     * @var string
     */
    protected $shortcut;

    /**
     * @var string
     */
    protected $longversion;

    /**
     * @var string
     */
    protected $shorttype;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var bool
     */
    protected $exclude;

    /**
     * @var string
     */
    protected $website;

    /**
     * Get short title
     *
     * @return string
     */
    public function getShort()
    {
        return $this->short;
    }

    /**
     * Set short title
     *
     * @param string $short
     */
    public function setShort($short)
    {
        $this->short = $short;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getShortcut()
    {
        return $this->shortcut;
    }

    /**
     * Set type
     *
     * @param string $shortcut
     */
    public function setShortcut($shortcut)
    {
        $this->shortcut = $shortcut;
    }

    /**
     * Get long explanation
     *
     * @return string
     */
    public function getLongversion()
    {
        return $this->longversion;
    }

    /**
     * Set long explanation
     *
     * @param string $longversion
     */
    public function setLongversion($longversion)
    {
        $this->longversion = $longversion;
    }

    /**
     * Get short type
     *
     * @return string
     */
    public function getShorttype()
    {
        return $this->shorttype;
    }

    /**
     * Set short type
     *
     * @param string $shorttype
     */
    public function setShorttype($shorttype)
    {
        $this->shorttype = $shorttype;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Exclude from markup
     *
     * @return boolean
     */
    public function getExclude()
    {
        return $this->exclude;
    }

    /**
     * Set exclude from markup
     *
     * @param boolean $exclude
     */
    public function setExclude($exclude)
    {
        $this->exclude = $exclude;
    }

    /**
     * Get external website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set external website
     *
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }
}
