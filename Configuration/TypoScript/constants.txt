plugin.tx_gbglossary {
	view {
		# cat=plugin.tx_gbglossary/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:gb_glossary/Resources/Private/Templates/
		# cat=plugin.tx_gbglossary/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:gb_glossary/Resources/Private/Partials/
		# cat=plugin.tx_gbglossary/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:gb_glossary/Resources/Private/Layouts/
	}

	persistence {
		# cat=plugin.tx_gbglossary//a; type=int+; label=Default storage PID
		storagePid =
	}
}
