<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

return [
    'ctrl' => [
        'title' => 'LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition',
        'label' => 'short',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioning' => '1',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l18n_parent',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'default_sortby' => 'ORDER BY short',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
            'fe_group' => 'fe_group',
        ],
        'typeicon_classes' => [
            'default' => 'content-image'
        ],
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid,l18n_parent,l18n_diffsource,hidden,starttime,endtime,fe_group,short,shortcut,longversion,shorttype,description,exclude, website'
    ],
    'feInterface' => [
        'fe_admin_fieldList' => 'sys_language_uid, l18n_parent, l18n_diffsource, hidden, starttime, endtime, fe_group, short, shortcut, longversion, shorttype, description, link, exclude, website',
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => [
                    ['LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1],
                    ['LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0]
                ]
            ]
        ],
        'l18n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_gbglossary_domain_model_definition',
                'foreign_table_where' => 'AND tx_gbglossary_domain_model_definition.pid=###CURRENT_PID### AND tx_gbglossary_domain_model_definition.sys_language_uid IN (-1,0)',
            ]
        ],
        'l18n_diffsource' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
            'config' => [
                'type' => 'check',
                'default' => '0'
            ]
        ],
        'starttime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => '8',
                'max' => '20',
                'eval' => 'date',
                'default' => '0',
                'checkbox' => '0'
            ]
        ],
        'endtime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => '8',
                'max' => '20',
                'eval' => 'date',
                'checkbox' => '0',
                'default' => '0',
                'range' => [
                    'upper' => mktime(0, 0, 0, 12, 31, 2037),
                    'lower' => mktime(0, 0, 0, date('m') - 1, date('d'), date('Y'))
                ]
            ]
        ],
        'fe_group' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.fe_group',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                    ['LLL:EXT:lang/locallang_general.xml:LGL.hide_at_login', -1],
                    ['LLL:EXT:lang/locallang_general.xml:LGL.any_login', -2],
                    ['LLL:EXT:lang/locallang_general.xml:LGL.usergroups', '--div--']
                ],
                'foreign_table' => 'fe_groups'
            ]
        ],
        'short' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.short',
            'config' => [
                'type' => 'input',
                'size' => '30',
                'eval' => 'required',
            ]
        ],
        'shortcut' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.shortcut',
            'config' => [
                'type' => 'input',
                'size' => '30',
            ]
        ],
        'longversion' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.longversion',
            'config' => [
                'type' => 'input',
                'size' => '48',
                'max' => '255',
                'eval' => 'tx_gbglossary_utility_evalstringlength',
            ]
        ],
        'shorttype' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.shorttype',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.shorttype.I.0', 'dfn'],
                    ['LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.shorttype.I.1', 'acronym'],
                    ['LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.shorttype.I.2', 'abbr'],
                ],
                'size' => 1,
                'maxitems' => 1,
            ]
        ],
        'description' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.description',
            'defaultExtras' => 'richtext:rte_transform[mode=ts_css]',
            'config' => [
                'type' => 'text',
                'cols' => '80',
                'rows' => '15',
                'wizards' => [
                    'RTE' => [
                        'notNewRecords' => 1,
                        'RTEonly' => 1,
                        'type' => 'script',
                        'title' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext.W.RTE',
                        'icon' => 'actions-wizard-rte',
                        'module' => [
                            'name' => 'wizard_rte'
                        ]
                    ],
                ],
                'softref' => 'typolink_tag,images,email[subst],url',
            ],
        ],
        'website' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.website',
            'config' => [
                'type' => 'input',
                'size' => '48',
                'max' => '255',
                'checkbox' => '',
                'eval' => 'trim',
                'wizards' => [
                    'link' => [
                        'type' => 'popup',
                        'title' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_link_formlabel',
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_link.gif',
                        'module' => [
                            'name' => 'wizard_link',
                        ],
                        'JSopenParams' => 'width=800,height=600,status=0,menubar=0,scrollbars=1'
                    ]
                ],
            ]
        ],
        'exclude' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.exclude',
            'config' => [
                'type' => 'check',
            ]
        ],
    ],
    'types' => [
        '0' => ['showitem' => 'sys_language_uid;;;;1-1-1, l18n_parent, l18n_diffsource, hidden;;1, short, shortcut, longversion, shorttype, description, link, exclude, website']
    ],
    'palettes' => [
        '1' => ['showitem' => 'starttime, endtime, fe_group'],
        '2' => ['showitem' => 'shortcut'],
    ]
];
